﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO;
namespace Tomograph
{
    //public struct Geometry
    //{
    //    public float DistanceSource2Detector;
    //    public float DistanceTable2Focus;
    //    public float DistanceTable2Detector;
    //    public float DectorXlength;
    //    public float DectorYlength;
    //}
    class math_Mod
    {
        public delegate void OneImageHandle(float percents);
        public event OneImageHandle NewImageFinished;
        float voxelSizeX;
        float voxelSizeY;
        float voxelSizeZ;
        int[] gridSize;
        float[] gridBounds;
        //TimeOnline m = new TimeOnline(); // Это для времени
        public math_Mod()
        {
        }
        public void SARTFullScale(int img_num, float[] angle, ushort[] img, float[] recon_data, int[] img_Size, int[] grid, float[] Bounds, Geometry geom, float relax = 1)
        {
            //m.Tim_Start();
            gridSize = grid;
            gridBounds = Bounds;
            voxelSizeX = (gridBounds[3] - gridBounds[0]) / gridSize[0]; 
            voxelSizeY = (gridBounds[4] - gridBounds[1]) / gridSize[1]; 
            voxelSizeZ = (gridBounds[5] - gridBounds[2]) / gridSize[2];
            int res = img_Size[0] * img_Size[1];
            int resgrid = grid[0] * grid[1] * grid[2];
            float focus2matrix = geom.DistanceTable2Focus + geom.DistanceTable2Detector;
            float emmiter2focus = geom.DistanceSource2Detector - focus2matrix;
            float[] vox_temp = new float[resgrid];
            float[] vox_mask = new float[resgrid];
            for (uint kk = 0; kk < resgrid; kk++)
            {
                vox_temp[kk] = 0;
                vox_mask[kk] = 1;
            }
            float X2centerPredv = (float)-Math.Tan((angle[0]) * Math.PI / 180.0f) * focus2matrix;
            float coordinates2 = -geom.DistanceTable2Detector;
            float coordinates3 = +(float)Math.Tan((angle[0]) * Math.PI / 180.0f) * emmiter2focus;
            float coordinates4 = 0;
            float coordinates5 = emmiter2focus + geom.DistanceTable2Focus;
            float xhvostPredv = X2centerPredv - geom.DectorXlength / 2;
            float yhvostPredv = geom.DectorYlength / 2;
            float xmnozPredv = geom.DectorXlength / img_Size[0];
            float ymnozPredv = geom.DectorYlength / img_Size[1];
            
            Parallel.For(0, res, cnt0 =>
            {
                int[] vox_ind_lin = new int[4 * gridSize[0]];
                int vox_num = 0;
                float[] coordinates = new float[6];
                coordinates[5] = coordinates5;
                coordinates[4] = coordinates4;
                coordinates[3] = coordinates3;
                coordinates[2] = coordinates2;
                int rowcnt = cnt0 / img_Size[1];
                int colcnt = cnt0 % img_Size[1];
                coordinates[0] = rowcnt * xmnozPredv + xhvostPredv;
                coordinates[1] = colcnt * ymnozPredv - yhvostPredv;
                traceFuncParallel(coordinates, vox_ind_lin, ref vox_num, cnt0);
                if (vox_num > 0)
                {
                    float n = 0.0f;
                    for (int vox = 0; vox < vox_num; vox++)
                    {
                        n += recon_data[vox_ind_lin [vox]];
                    }
                    n = (img[cnt0] - n) / vox_num;
                    for (int vox = 0; vox < vox_num; vox++)
                    {
                        vox_mask[vox_ind_lin[vox]]++;
                        vox_temp[vox_ind_lin[vox]] += n;
                    }
                }
            });
            
            //NewImageFinished (100); //Загадочно
            for(uint kk = 0; kk < resgrid; kk++)
            {
                recon_data[kk] += vox_temp[kk] / vox_mask[kk];
                if (recon_data[kk] < 0)
                    recon_data[kk] = 0;
            }
            //m.Tim_Stop("SARTFullScale Time");
        }
        void traceFuncParallel(float[] lineCoords, int[] indexes, ref int sizeofInd, /*float[] length2_inv,*/ int ppcnt)
        {
            // Trace a line segment defined by two points through a voxel grid (using Woo's raytracing algorithm).
            // Determine linear indexes of voxels intersected by the line segment.

            // Inputs
            //First input: dimensions of the voxel grid [Nx Ny Nz]
            //Second input: a 1-by-6 matrix with voxel grid boundaries [xMin yMin zMin xMax yMax zMax] (must satisfy xMax>xMin, yMax>yMin, zMax>zMin)
            //Third input: a 1-by-6 matrix with line segment coordinates [x1,y1,z1,x2,y2,z2]

            float[] lineCoord = lineCoords;
            float tDeltaX, tDeltaY, tDeltaZ; // parametric step size along different dimensions
            float tMaxX, tMaxY, tMaxZ; // used to determine the dimension of the next step along the line
            float tMin, tMax; // maximum and minimum parameteric coordinates
            int X, Y, Z; // voxel subscripts
            int stepX, stepY, stepZ; // direction of grid traversal
            int storageSize; //initial storage size - dynamically updated
            int addedVoxelCount;
            tMin = 0; tMax = 0;
            float xStart, yStart, zStart, xEnd, yEnd, zEnd; // point where segment intersects the grid

            float[] vec = new float[3], vec_norm = new float[3];
            float norm = 0;
            for (int k = 0; k < 3; k++)
            {
                vec[k] = (lineCoord[3+k] - lineCoord[k]);
                norm += vec[k] * vec[k];
            }

            norm = (float)Math.Sqrt(norm);

            for (int k = 0; k < 3; k++)
            {
                vec_norm[k] = vec[k]/norm;
                vec[k] = vec_norm[k];
            }

            // Intersection test, find minimum and maximum parameteric intersection coordinate
            bool intersectTest = boxIntersectTest(gridBounds, lineCoord, vec, out tMin, out tMax);
            if (!intersectTest)
            {
                return;
            }

            xStart = lineCoord[0] + vec[0] * tMin;
            yStart = lineCoord[1] + vec[1] * tMin;
            zStart = lineCoord[2] + vec[2] * tMin;
            xEnd = lineCoord[0] + vec[0] * tMax;
            yEnd = lineCoord[1] + vec[1] * tMax;
            zEnd = lineCoord[2] + vec[2] * tMax;

            //int intersectedVoxelDataLength = (int)4 * (int)gridSize[0];

            // Allocate memory to store the indexes of the intersected voxels
            storageSize = 4 * gridSize[0];

            //intersectedVoxelData = (int*)mxGetData(plhs[0]);

            // Determine initial voxel coordinates and line traversal directions
            // X-dimension
            X = (int)Math.Max(0, Math.Ceiling((xStart - gridBounds[0]) / voxelSizeX));  // starting coordinate - include left boundary - index starts from 1
            //Xend = (int)Math.Max(0, Math.Ceiling((xEnd - gridBounds[0]) / voxelSizeX)); // ending coordinate - stepping continues until we hit this index
            if (Math.Abs(vec[0]) != 0)
            {
                stepX = Math.Sign(vec[0]);
                tDeltaX = voxelSizeX / Math.Abs(vec[0]); //parametric step length between the x-grid planes
                tMaxX = tMin + (gridBounds[0] + X * voxelSizeX - xStart) / vec[0]; // parametric distance until the first crossing with x-grid plane
                //tMaxX = tMin;
            }
            else
            {
                stepX = 0;
                tMaxX = tMax; // the line doesn't cross the next x-plane
                tDeltaX = tMax; // set the parametric step to maximum
            }
            // Y-dimension
            Y = (int)Math.Max(0, Math.Ceiling((yStart - gridBounds[1]) / voxelSizeY));
            //Yend = (int)Math.Max(0, Math.Ceiling((yEnd - gridBounds[1]) / voxelSizeY));
            if (Math.Abs(vec[1]) != 0)
            {
                stepY = Math.Sign(vec[1]);
                tDeltaY = voxelSizeY / Math.Abs(vec[1]); //parametric step length between the x-grid planes
                tMaxY = tMin + (gridBounds[1] + Y * voxelSizeY - yStart) / vec[1]; // parametric distance until the first crossing with x-grid plane
                //tMaxX = tMin;
            }
            else
            {
                stepY = 0;
                tMaxY = tMax;
                tDeltaY = tMax;
            }
            // Z-dimension
            Z = (int)Math.Max(0, Math.Ceiling((zStart - gridBounds[2]) / voxelSizeZ));
            //Zend = (int)Math.Max(0, Math.Ceiling((zEnd - gridBounds[2]) / voxelSizeZ));
            if (Math.Abs(vec[2]) != 0)
            {
                stepZ = Math.Sign(vec[2]);
                tDeltaZ = voxelSizeZ / Math.Abs(vec[2]); //parametric step length between the x-grid planes
                tMaxZ = tMin + (gridBounds[2] + Z * voxelSizeZ - zStart) / vec[2]; // parametric distance until the first crossing with x-grid plane
                //tMaxX = tMin;
            }
            else
            {
                stepZ = 0;
                tMaxZ = tMax;
                tDeltaZ = tMax;
            }
            // Add initial voxel to the list
            indexes[0] = sub2ind(X, Y, Z);
            addedVoxelCount = 1;

            // Step iteratively through the grid
            while (true)
            {
                if (tMaxX < tMaxY)
                {
                    if (tMaxX < tMaxZ)
                    {
                        X += stepX;
                        tMaxX += tDeltaX;
                    }
                    else
                    {
                        Z += stepZ;
                        tMaxZ += tDeltaZ;
                    }
                }
                else
                {
                    if (tMaxY < tMaxZ)
                    {
                        Y += stepY;
                        tMaxY += tDeltaY;
                    }
                    else
                    {
                        Z += stepZ;
                        tMaxZ += tDeltaZ;
                    }
                }

                if ((X < 0) || (Y < 0) || (Z <0))
                    break;

                if ((X >= gridSize[0]) || (Y >= gridSize[1]) || (Z >= gridSize[2]))
                    break;

                addedVoxelCount++;

                //must perform memory check - if the initial allocated array is large enough this step is not necessary
                if (addedVoxelCount > storageSize)
                {
                    return;
                }
                indexes[addedVoxelCount - 1] = sub2ind(X, Y, Z);
               // float buf1 = (X - gridSize[0] / 2) * voxelSizeX;
                //float buf2 = (Y - gridSize[1] / 2) * voxelSizeY;
                //float buf3 = Z * voxelSizeZ + 80;
                //length2_inv[addedVoxelCount - 1] = (float)(1.0f /(4*Math.PI*(buf1 * buf1 + buf2 * buf2 + buf3 * buf3)) );

            }
            sizeofInd = addedVoxelCount;

        }
        int sub2ind(int X, int Y, int Z)
        {
            return (int)(X + (Y) * gridSize[0] + (Z) * gridSize[1] * gridSize[0]);
        }
        bool boxIntersectTest(float[] gridBounds, float[] lineCoord, float[] vec, out float tMinR, out float tMaxR)
        {
            float tNear = float.NegativeInfinity, tFar = float.PositiveInfinity, tMin, tMax;
            float div;
            float buf;
            tMinR = 0;
            tMaxR = 0;

            for (int i = 0; i< 3; i++)
            {
                if ((vec[i]) != 0)
                {
                    div = 1/vec[i];

                    tMin = (gridBounds[0+i] - lineCoord[0 + i]) * div;
                    tMax = (gridBounds[3] - lineCoord[0 + i]) * div;

                    if (tMin > tMax)
                    {                        
                        buf = tMin;
                        tMin = tMax;
                        tMax = buf;
                    }

                    if (tMin > tNear)
                    {
                        tNear = tMin;
                    }
                    if (tMax < tFar)
                    {
                        tFar = tMax;
                    }

                    if ((tFar < tNear) || (tFar < 0))
                        return false;

                }
            }
            tMinR = tNear;
            tMaxR = tFar;
             
            return true;

        }
    }
}

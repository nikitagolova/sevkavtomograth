﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Drawing;

using System.Threading;

namespace Tomograph
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        const int img_SizeX = 1216;//304; //1024 //128
        const int img_SizeY = 987;//247; //1024 //128

        static int[] gridBounds = new int[3] { 512, 512, 512 };
        //static float[] gridSize = new float[6] { -215, -215, 0, 215, 215, 430 };
        static float[] gridSize = new float[6] { -150, -150, 0, 150, 150, 300};

        static ushort[] imgData = new ushort[img_SizeX * img_SizeY * 61];
        static float[] recon;
        static Thread mythread;

        static int numOfProjections = 1;
         
        static math art = new math();
        static math_Mod SART = new math_Mod();

        private System.ComponentModel.BackgroundWorker backgroundWorker1 = new System.ComponentModel.BackgroundWorker();

        static Random rnd = new Random();

        public static float Gauss(float center, float sigma)
        {
            return (float)(center + sigma * (rnd.NextDouble() + rnd.NextDouble() + rnd.NextDouble() + rnd.NextDouble() - 2) / 2);
        }

        static void fornewthread()
        {
            float[] angle = new float[61];
            int[] iter = new int[61];

            for (int i = 0; i < 61; i++)
            {
                angle[i] = i - 30;
                iter[i] = i;
            }

            Random random = new Random();

            Geometry st = new Geometry();
            st.DectorXlength = 459.5f;
            st.DectorYlength = 383.5f;
            st.DistanceSource2Detector = 1800;
            st.DistanceTable2Focus = 100;
            st.DistanceTable2Detector = 80;

            for (int i = (iter.Length - 1); i >= 1; i--)
            {
                int j = random.Next(i + 1);
                // обменять значения data[j] и data[i]
                var temp = iter[j];
                iter[j] = iter[i];
                iter[i] = temp;
            }

            //for (UInt32 i = 0; i < gridBounds[0] * gridBounds[1] * gridBounds[2]; i++)
            //recon[i] = 0;
            float[] relax = new float[3] {1.0f, 0.8f, 0.5f };
            int[] img_Size = new int[2] { img_SizeX, img_SizeY };

            

            for (int k = 0; k < 1; k++)
            {

                for (int i = 0; i < numOfProjections; i++)
                {
                    float[] an = new float[1];
                    //iter[i] = 31;s
                    an[0] = angle[iter[i]];// + Gauss(0,1f);

                    ushort[] img = new ushort[img_SizeX * img_SizeY];

                    for (uint j = 0; j < img.Length; j++)
                        img[j] = imgData[j + img.Length * iter[i]];

                    //art.ARTFullScale(numOfProjections, angle, imgData, recon, img_Size, gridBounds, gridSize);
                    //art.SARTFullScale(1, an, img, recon, img_Size, gridBounds, gridSize, st, relax[k]);

                    SART.SARTFullScale(1, an, img, recon, img_Size, gridBounds, gridSize, st, relax[k]);
                }
            }

            FileStream mf = new FileStream(@"..\..\recon_small_ph.bin", FileMode.Create);

            BinaryWriter wr = new BinaryWriter(mf);

            for (int i = 0; i < recon.Length; i++)
            {
                wr.Write(recon[i]);
            }

            mf.Close();

            mythread.Abort();
        }
        

        public MainWindow()
        {
            InitializeComponent();

            //FileStream mf = new FileStream(@"..\..\img2.dat", FileMode.Open);
            FileStream mf = new FileStream(@"..\..\Phantom_800_40.bin", FileMode.Open); 
            BinaryReader reader = new BinaryReader(mf);

            for (int i = 0; i < imgData.Length; i++)
            {
                //imgData[i] = (ushort) (0xFFFF - reader.ReadUInt16());
                imgData[i] = (ushort)(reader.ReadUInt16());
            }

            ushort max = imgData.Max();
            float buf;
            Parallel.For(0, imgData.Length, i =>//for (int i = 0; i < imgData.Length; i++)
            {
                buf = 1 - (float)imgData[i] / max;
                //buf = (float)imgData[i] / max;
                imgData[i] = (ushort)(buf * 0xFFFF);
            });

            //ushort[,] res = new ushort[1024, 1024];


            image.Source = GetBitmapforImage(imgData, 0);
            image1.Source = GetBitmapforImage(imgData, 25);
            image2.Source = GetBitmapforImage(imgData, 35);
            image3.Source = GetBitmapforImage(imgData, 60);

            art.NewImageFinished += Art_NewImageFinished;
            SART.NewImageFinished += Art_NewImageFinished;
            ProgBar.Minimum = 0;
            ProgBar.Maximum = 100;
            ProgBar.Value = 0;
            recon = new float[gridBounds[0] * gridBounds[1] * gridBounds[2]];

        }

        private void Art_NewImageFinished(float percents)
        {
            this.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Background, new System.Windows.Threading.DispatcherOperationCallback(delegate
            {
                ProgBar.Value = percents;
                if (percents == 100)
                    image4.Source = GetBitmapforReconstruction(recon, (int)Convert.ToDecimal(textBox.Text));
                return null;

            }), null);
        }

        private BitmapImage GetBitmapforImage(ushort[] data, int num)
        {
            Bitmap g = new Bitmap(img_SizeX, img_SizeY);
            g.SetResolution(img_SizeX, img_SizeY);
            ushort max = imgData.Max();

            for (int i = 0; i < img_SizeX; i++)
                for (int j = 0; j < img_SizeY; j++)
                {
                    float d = (float)imgData[j + i * img_SizeY + num * img_SizeX * img_SizeY] / max * 255;
                    g.SetPixel(i, j, System.Drawing.Color.FromArgb((int)d, (int)d, (int)d));
                }

            BitmapImage bitmapimage = new BitmapImage();
            using (MemoryStream memory = new MemoryStream())
            {
                g.Save(memory, System.Drawing.Imaging.ImageFormat.Bmp);
                memory.Position = 0;
                bitmapimage.BeginInit();
                bitmapimage.StreamSource = memory;
                bitmapimage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapimage.EndInit();
            }

            return bitmapimage;
        }

        private BitmapImage GetBitmapforReconstruction(float[] data, int num)
        {
            if (num < 1)
                num = 1;
            if (num > gridBounds[2])
                num = gridBounds[2];

            Bitmap g = new Bitmap(gridBounds[0], gridBounds[1]);
            g.SetResolution(gridBounds[0], gridBounds[1]);
            float[] slice = new float[gridBounds[0] * gridBounds[1]];
            for (int i = 0; i < gridBounds[0]; i++)
                for (int j = 0; j < gridBounds[1]; j++)
                {
                    slice[i + j * gridBounds[0]] = data[i + j * gridBounds[0] + (num - 1) * gridBounds[0] * gridBounds[1]];
                }

            //float max = (float)slice.Max();
            //float min = (float)slice.Min();
            float max = (float)data.Max();
            float min = (float)data.Min();
            if ((max > 0) && (num < gridBounds[2]))
            {
                for (int i = 0; i < gridBounds[0]; i++)
                    for (int j = 0; j < gridBounds[1]; j++)
                    {
                        float d = (float)(slice[i + j * gridBounds[0]] - min) / (max - min) * 255;
                        g.SetPixel(i, j, System.Drawing.Color.FromArgb((int)d, (int)d, (int)d));
                    }
            }

            BitmapImage bitmapimage = new BitmapImage();
            using (MemoryStream memory = new MemoryStream())
            {
                g.Save(memory, System.Drawing.Imaging.ImageFormat.Bmp);
                memory.Position = 0;
                bitmapimage.BeginInit();
                bitmapimage.StreamSource = memory;
                bitmapimage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapimage.EndInit();
            }

            return bitmapimage;
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            ProgBar.Value = 0;
            mythread = new Thread(fornewthread);
            mythread.Priority = ThreadPriority.Normal;
            mythread.IsBackground = true;

            if ((int)Convert.ToDecimal(textBox1.Text) < 62)
            {
                numOfProjections = (int)Convert.ToDecimal(textBox1.Text);
                mythread.Start();
            }
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            image4.Source = GetBitmapforReconstruction(recon, (int)Convert.ToDecimal(textBox.Text));
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            textBox.Text = ((int)(Convert.ToDecimal(textBox.Text) + 1)).ToString();
            if ((int)(Convert.ToDecimal(textBox.Text)) > 350)
                textBox.Text = (350).ToString();
            else if ((int)(Convert.ToDecimal(textBox.Text)) < 0)
                textBox.Text = (0).ToString();

            image4.Source = GetBitmapforReconstruction(recon, (int)Convert.ToDecimal(textBox.Text));
        }

        private void button3_Click(object sender, RoutedEventArgs e)
        {
            textBox.Text = ((int)(Convert.ToDecimal(textBox.Text) - 1)).ToString();
            if ((int)(Convert.ToDecimal(textBox.Text)) > gridBounds[2])
                textBox.Text = gridBounds[2].ToString();
            else if ((int)(Convert.ToDecimal(textBox.Text)) < 0)
                textBox.Text = (0).ToString();

            image4.Source = GetBitmapforReconstruction(recon, (int)Convert.ToDecimal(textBox.Text));
        }
    }
}

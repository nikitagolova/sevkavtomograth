﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SevKav
{
    class ReadSomeFileEntity
    {
        public Bitmap[] _bmpFile { get; private set; }

        public ReadSomeFileEntity()
        {
            dcmReader = new DICOMReader();
        }

        public DicomParametrs OpenFilesEntity(int projection)
        {
            var fbd = new FolderBrowserDialog();

            if (fbd.ShowDialog() != DialogResult.OK)
            {
                return null;
            }

            if (fbd.SelectedPath  != OldDirectory)
            {
                FullfilesPath = Directory.GetFiles(fbd.SelectedPath, "*.dcm"); //массив директорий DICOM файлов на диске
            }
            
            if (FullfilesPath.Length == 0)
            {
                return null;
            }

            if (projection > FullfilesPath.Length)
            {
                projection = FullfilesPath.Length;
            }

            var dicomParametrs = dcmReader.OpenSomeDicomFiles(FullfilesPath, projection);

            if (dicomParametrs == null)
            {
                return null;
            }

            _bmpFile = dcmReader.CreateBitmap(dicomParametrs._projection);

            return dicomParametrs;
        }

        private DICOMReader dcmReader;
        private string OldDirectory { get; set; }
        private string[] FullfilesPath { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;
using System.Drawing.Imaging;

namespace SevKav
{

    public class ReadFileEntity
    {
        public Bitmap[] _bmpFile { get; private set; } 

        public ReadFileEntity()
        {
            dcmReader = new DICOMReader();
        }

        public DicomParametrs OpenFileEntity()
        {
            var ofd = new OpenFileDialog();
            var dicomParametrs = new DicomParametrs();
            //отображать предупреждение, если пользователь указывает несуществующий путь
            ofd.CheckPathExists = true;
            //отображается кнопка "Справка" в диалоговом окне
            ofd.ShowHelp = true;
            //Настраиваем фильтр на необходимые форматы файлов
            ofd.Filter = "DICOM files (*.DCM)|*.DCM|RAW Files(*.RAW)|*.RAW";

            if (ofd.ShowDialog() != DialogResult.OK)
            {
                return null;
            }

            if (ofd.FilterIndex == 2)
            {
                //_bmpFile = ReadRawFile(ofd);
            }
            else if (ofd.FilterIndex == 1)
            {
                dicomParametrs = ReadDicomFile(ofd);
            }

            return dicomParametrs;
        }

        private DICOMReader dcmReader;

        private Bitmap ReadRawFile(OpenFileDialog ofd)
        {
            var _massivRawByte = File.ReadAllBytes(ofd.FileName);
            
            int _widthRaw = 3040;
            int _heightRaw = 2466;

            var bmpRaw = new Bitmap(_widthRaw, _heightRaw, PixelFormat.Format48bppRgb);
            var rectangle = new Rectangle(0, 0, _widthRaw, _heightRaw);
            var bmpData = bmpRaw.LockBits(rectangle, ImageLockMode.WriteOnly, bmpRaw.PixelFormat);

            var _ptr = bmpData.Scan0;

            // объявление байтового массива размером с изображение
            var bytes = bmpData.Stride * bmpRaw.Height;
            var rgbValues = new byte[bytes];

            var _massivRawPixel = new ushort[bytes];

            int k = 0;

            for (int i = 0; i < rgbValues.Length; i += 6)
            {
                rgbValues[i + 5] = (byte)(_massivRawByte[k + 1]);
                rgbValues[i + 4] = (byte)(_massivRawByte[k]);
                rgbValues[i + 3] = (byte)(_massivRawByte[k + 1]);
                rgbValues[i + 2] = (byte)(_massivRawByte[k]);
                rgbValues[i + 1] = (byte)(_massivRawByte[k + 1]);
                rgbValues[i] = (byte)(_massivRawByte[k]);

                _massivRawPixel[k / 2] = (ushort)(_massivRawByte[k] * 256 + _massivRawByte[k + 1]);

                k += 2;
            }

            //dicomParametrs._massivFileData = _massivRawPixel;

            //копирование матрицы обратнов изображение
            System.Runtime.InteropServices.Marshal.Copy(rgbValues, 0, _ptr, bytes);

            bmpRaw.UnlockBits(bmpData);
            bmpRaw.RotateFlip(RotateFlipType.Rotate270FlipXY);
            return bmpRaw;
        }

        private DicomParametrs ReadDicomFile(OpenFileDialog ofd)
        {
            var dicomParametrs = dcmReader.OpenDicomFile(ofd.FileName);

            if (dicomParametrs == null)
            {
                return null;
            }

            var _bmpDicom = dcmReader.CreateBitmap(dicomParametrs._projection);

            _bmpFile = _bmpDicom;

            return dicomParametrs;
        }
    }
}

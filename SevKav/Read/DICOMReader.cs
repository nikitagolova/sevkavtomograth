﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SevKav
{
    public class DicomParametrs
    {
        public string _name { get; set; }
        public int _projection { get; set; }
        public int _rows { get; set; }
        public int _collums { get; set; }
        public int _samples_Per_Pixel { get; set; }
        public int _bits_Allocated { get; set; }
        public int _bits_Stored { get; set; }
        public int _pixel_Representation { get; set; }
        public ushort[] _massivFileData { get; set; }
        public List<int> _angle { get; set; }
        public List<int> winMax;
        public List<int> winMin;
    }

    class DICOMReader 
    {
        public DicomParametrs _dicomParametrs { get; private set; }

        public DICOMReader()
        {
            _dicomParametrs = new DicomParametrs()
            {
                _angle = new List<int>(),
                winMax = new List<int>(),
                winMin = new List<int>()
            };

            factor = new List<double>();
        }

        public DicomParametrs OpenDicomFile(string path)
        {
            ListClear();

            if (string.IsNullOrEmpty(path))
            {
                return null;
            }

            var allBits = ReadDicomFile(path);
            var mas1 = new ushort[allBits.Length / 2];
            var k = 0;

            for (int i = 0; i < allBits.Length - 1; i += 2)
            {
                mas1[k] = BitConverter.ToUInt16(allBits, i);
                k++;
            }

            _dicomParametrs._projection = 1;
            _dicomParametrs.winMax.Add(mas1.Max());
            _dicomParametrs.winMin.Add(mas1.Min());

            int range = _dicomParametrs.winMax[0] - _dicomParametrs.winMin[0];
            if (range < 1) range = 1;
            factor.Add(255.0 / range);

            _dicomParametrs._massivFileData = mas1;

            return _dicomParametrs;
        }

        public DicomParametrs OpenSomeDicomFiles(string[] paths, int projection)
        {
            ListClear();
            var collums = CheckCollums(paths);
            var rows = CheckRows(paths);
            //var outPut = new List<ushort>();
            _dicomParametrs._massivFileData = new ushort[collums * rows * projection];
            var k = 0;

            for (int i = 0; i < projection; i++) 
            {
                var oneTime = OpenDicomFile(paths[i]);
                //var mas1 = new ushort[oneTime.Length / 2];
                //var k = 0;

                for (int j = 0; j < oneTime ._massivFileData.Length - 1; j+=2)
                {
                    _dicomParametrs._massivFileData[k] = oneTime._massivFileData[j];
                    k++;
                }

                //outPut.AddRange(mas1);
                _dicomParametrs.winMax.Add(0);
                _dicomParametrs.winMin.Add(65536);
                var range = _dicomParametrs.winMax[i] - _dicomParametrs.winMin[i];
                if (range < 1) range = 1;
                factor.Add(255.0 / range);
            }

            _dicomParametrs._projection = projection;
            //_dicomParametrs._massivFileData = outPut.ToArray();

            return _dicomParametrs;
        }

        public Bitmap[] CreateBitmap(int projection)
        {
            //var _bmp = new Bitmap(_dicomParametrs._collums, _dicomParametrs._rows, PixelFormat.Format48bppRgb);
            var _bmp = new Bitmap[projection];

            for (int p = 0; p < projection; p++)
            {
                _bmp[p] = new Bitmap(_dicomParametrs._collums, _dicomParametrs._rows, PixelFormat.Format48bppRgb);
                var bmd = _bmp[p].LockBits(new Rectangle(0, 0, _dicomParametrs._collums, _dicomParametrs._rows), ImageLockMode.ReadOnly, _bmp[p].PixelFormat);

                var picturenumber = _dicomParametrs._collums * _dicomParametrs._rows * p;

                if (picturenumber > _dicomParametrs._massivFileData.Length)
                {
                    break;
                }

                var massiv = new ushort[_dicomParametrs._collums * _dicomParametrs._rows];

                for (int i = 0; i < massiv.Length; i++)
                {
                    massiv[i] = _dicomParametrs._massivFileData[i + picturenumber];
                }

                IntPtr xx;

                var pixelsvalue = 6;
                var bytes = massiv.Length * pixelsvalue;
                var rgbValues = new byte[bytes];
                var jk = 0;

                for (int i = 0; i < bmd.Height; i++)
                {
                    xx = bmd.Scan0 + (i * bmd.Stride);

                    for (int j = 0; j < bmd.Width; j++)
                    {
                        var result = BitConverter.GetBytes((massiv[i * bmd.Width + j] - _dicomParametrs.winMin[p])/* * factor[p]*/);
                        rgbValues[jk + 0] = result[0];
                        rgbValues[jk + 1] = result[1];
                        rgbValues[jk + 2] = result[0];
                        rgbValues[jk + 3] = result[1];
                        rgbValues[jk + 4] = result[0];
                        rgbValues[jk + 5] = result[1];

                        jk += pixelsvalue;
                    }

                    System.Runtime.InteropServices.Marshal.Copy(rgbValues, i * bmd.Width * pixelsvalue, xx, bmd.Width * pixelsvalue);
                }

                _bmp[p].UnlockBits(bmd);                

                /*Изменение размера проекции и сохранение ее в директорию исполняемого файла*/
                var s = _bmp[p].Size;
                s.Width = 200;
                s.Height = 200;
                _bmp[p] = new Bitmap(_bmp[p], s);
                //bmp.Save("pic" + p + ".bmp");
                
                //bmp.Dispose();
                //Очистка памяти от "мусора"
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
            
            ListClear();

            return _bmp;
        }

        private List<double> factor;

        private void ListClear()
        {
            _dicomParametrs.winMin.Clear();
            _dicomParametrs.winMax.Clear();
            factor.Clear();
        }

        private byte[] ReadDicomFile(string Path)
        {
            EvilDicom.Components.DICOMFile dcmf = new EvilDicom.Components.DICOMFile(Path);
            EvilDicom.Components.DICOMElement dICOMElement = new EvilDicom.Components.DICOMElement();

            var tName = dcmf.Find(EvilDicom.Helper.TagHelper.PATIENT_NAME) as EvilDicom.VR.PersonsName;
            _dicomParametrs._name = tName.Data;

            var tCollums = dcmf.Find(EvilDicom.Helper.TagHelper.COLUMNS) as EvilDicom.VR.UnsignedShort;
            _dicomParametrs._collums = tCollums.Data;
            var trows = dcmf.Find(EvilDicom.Helper.TagHelper.ROWS) as EvilDicom.VR.UnsignedShort;
            _dicomParametrs._rows = trows.Data;

            var tspp = dcmf.Find(EvilDicom.Helper.TagHelper.SAMPLES_PER_PIXEL) as EvilDicom.VR.UnsignedShort;
            _dicomParametrs._samples_Per_Pixel = tspp.Data;

            var tba = dcmf.Find(EvilDicom.Helper.TagHelper.BITS_ALLOCATED) as EvilDicom.VR.UnsignedShort;
            _dicomParametrs._bits_Allocated = tba.Data;
            var tbs = dcmf.Find(EvilDicom.Helper.TagHelper.BITS_STORED) as EvilDicom.VR.UnsignedShort;
            _dicomParametrs._bits_Stored = tbs.Data;
            var tpr = dcmf.Find(EvilDicom.Helper.TagHelper.PIXEL_REPRESENTATION) as EvilDicom.VR.UnsignedShort;
            _dicomParametrs._pixel_Representation = tpr.Data;

            var tpd = dcmf.Find(EvilDicom.Helper.TagHelper.PIXEL_DATA) as EvilDicom.VR.PixelData;
            var _pixel_Data = tpd.ByteData;
            var tmin = dcmf.Find(EvilDicom.Helper.TagHelper.CHANNEL_MAXIMUM_VALUE) as EvilDicom.VR.IntegerString;

            return _pixel_Data;
        }

        private int CheckCollums(string[] Paths)
        {
            int ChCollums = 0;
            bool flag = true;

            foreach (string Path in Paths)
            {
                EvilDicom.Components.DICOMFile dcmf = new EvilDicom.Components.DICOMFile(Path);
                EvilDicom.Components.DICOMElement dICOMElement = new EvilDicom.Components.DICOMElement();

                var tCollums = dcmf.Find(EvilDicom.Helper.TagHelper.COLUMNS) as EvilDicom.VR.UnsignedShort;
                var Collums = tCollums.Data;

                if (flag)
                {
                    ChCollums = (int)Collums;
                    flag = false;
                }

                //if (ChCollums != (int)Collums) ;
                //Здесь выдавать ошибку
                //MessageBox.Show("Не совпадают разрешения выбранных Dicom файлов");
            }
            return ChCollums;
        }

        private int CheckRows(string[] Paths)
        {
            int ChRows = 0;
            bool flag = true;

            foreach (string Path in Paths)
            {
                EvilDicom.Components.DICOMFile dcmf = new EvilDicom.Components.DICOMFile(Path);
                EvilDicom.Components.DICOMElement dICOMElement = new EvilDicom.Components.DICOMElement();

                var trows = dcmf.Find(EvilDicom.Helper.TagHelper.ROWS) as EvilDicom.VR.UnsignedShort;
                var Rows = trows.Data;

                if (flag)
                {
                    ChRows = (int)Rows;
                    flag = false;
                }

                if (ChRows != (int)Rows) ;
                //Здесь выдавать ошибку
                //MessageBox.Show("Не совпадают разрешения выбранных Dicom файлов");
            }
            return ChRows;
        }

        private byte[] TreatmentDCM(string Path)
        {
            EvilDicom.Components.DICOMFile dcmf = new EvilDicom.Components.DICOMFile(Path);
            EvilDicom.Components.DICOMElement dICOMElement = new EvilDicom.Components.DICOMElement();

            var tpd = dcmf.Find(EvilDicom.Helper.TagHelper.PIXEL_DATA) as EvilDicom.VR.PixelData;
            var Pixel_Data = tpd.ByteData;

            return Pixel_Data;
        }
    }
}

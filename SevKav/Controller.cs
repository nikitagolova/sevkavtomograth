﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SevKav
{
    public class Controller
    {
        public Controller()
        {
            readFile = new ReadFileEntity();
            readSomeFile = new ReadSomeFileEntity();
        }

        public Bitmap[] MainForm_OpenSomeFile(int projection)
        {
            var dicomParametrs = readSomeFile.OpenFilesEntity(projection);
            return readSomeFile._bmpFile;
        }

        public Bitmap[] MainForm_OpenFile()
        {
            var dicomParametrs = readFile.OpenFileEntity();
            return readFile._bmpFile;
        }

        private ReadSomeFileEntity readSomeFile;
        private ReadFileEntity readFile;
    }
}

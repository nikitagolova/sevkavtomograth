﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace SevKav
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            controller = new Controller();
        }

        private void btOpenFile_Click(object sender, EventArgs e)
        {
            var bitmap = controller.MainForm_OpenFile();

            if (bitmap == null)
            {
                return;
            }

            pictureBox1.Image = bitmap[0];
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            buttonRun.Enabled = true;
        }

        private Controller controller;
        private PictureBox[] pic;

        private void buttonDCMOpenFile_Click(object sender, EventArgs e)
        {
            if (!_isStart)
            {
                DeleteImageResourse();
            }

            projection = (int)Convert.ToDecimal(numericUpDownProjection.Value);

            var bitmap = controller.MainForm_OpenSomeFile(projection);

            if (bitmap == null)
            {
                return;
            }

            pictureBox1.Image = bitmap[0];
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;

            pic = new PictureBox[projection];

            for (int i = 0; i < projection; i++)
            {                                      
                pic[i] = new PictureBox();
                pic[i].Name = "Pic" + i;
                pic[i].Size = new Size(50, 50);
                pic[i].Click += Pic_Click;
                flowLayoutPanelSliderImage.Controls.Add(pic[i]);
                pic[i].Image = bitmap[i];
                //pic[i].Load("pic" + i + ".bmp");
                pic[i].SizeMode = PictureBoxSizeMode.StretchImage;

                //Очистка памяти от "мусора"
                GC.Collect();
                GC.WaitForPendingFinalizers();

                progressBarImage.Value = i;
            }

            buttonRun.Enabled = true;
            progressBarImage.Value = 0;
        }

        private int projection; //Кол-во проекций
        private bool _isStart = false;

        private void Pic_Click(object sender, EventArgs e)
        {
            //Вывод из слайдера по клику на pictureBox
            pictureBox1.Image = (sender as PictureBox).Image;
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
        }

        private void btRun_Click(object sender, EventArgs e)
        {
            //int[] gridBounds = new int[3];   //Массив границ вокселя
            //gridBounds[0] = (int)Convert.ToDecimal(textBoxVoxelX.Text); //По оси X
            //gridBounds[1] = (int)Convert.ToDecimal(textBoxVoxelY.Text); //По оси Y
            //gridBounds[2] = (int)Convert.ToDecimal(textBoxVoxelZ.Text); //По оси Z
            //float[] gridSize = new float[6]; //Массив размеров вокселя
            //gridSize[0] = -(int)Convert.ToDecimal(textBoxSizeX.Text) / 2;
            //gridSize[1] = -(int)Convert.ToDecimal(textBoxSizeX.Text) / 2;
            //gridSize[2] = 0;
            //gridSize[3] = (int)Convert.ToDecimal(textBoxSizeX.Text) / 2;
            //gridSize[4] = (int)Convert.ToDecimal(textBoxSizeX.Text) / 2;
            //gridSize[5] = -(int)Convert.ToDecimal(textBoxSizeX.Text);
            //int Projection = (int)Convert.ToDecimal(numericUpDownProjection.Value); //Кол-во проекций
            //int Layer = (int)Convert.ToDecimal(numericUpDownLayer.Value);   //Номер слоя реконструкции
            //int[] img_Size = new int[2]; // Массив расширений проекций
            //img_Size[0] = Width;
            //img_Size[1] = Height;
            //float SizeX = float.Parse(textBoxSizeWidth.Text);   //Размер детектора по оси X
            //float SizeY = float.Parse(textBoxSizeHeight.Text);  //Размер детектора по оси Y
            //int DistDetector = Int32.Parse(textBoxDistDetector.Text);   //Растояние до детектора
            //recon = new float[gridBounds[0] * gridBounds[1] * gridBounds[2]];   //Массив реконструированного изображения

            //float[] angle = new float[61];  //Массив угла наклона излучателя от -30 до 30 градусов

            ////Заполнение массива угла наклона
            //for (int i = 0; i < 61; i++)
            //    angle[i] = i - 30;

            ////Обнуление массива
            //for (UInt32 i = 0; i < gridBounds[0] * gridBounds[1] * gridBounds[2]; i++)
            //    recon[i] = 0;
            ////Выбор алгоритма реконструкции
            //if (tabMetods.SelectedTab.ToString() == "TabPage: {ART}") 
            //{
            //    for (int i = 0; i < Projection; i++) 
            //    {
            //        float[] an = new float[1];
            //        an[0] = angle[i];
            //        //Вызов алгоритма реконструкции ART
            //        //art.SARTFullScale(Projection, an, MassivPixels, ref recon, img_Size, gridBounds, gridSize, SizeX, SizeY, DistDetector);
            //    }
            //    //pictureBox2.Image = draw.ArtDraw(recon, gridBounds, Layer);
            //    pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage;
            //}
            //if (tabMetods.SelectedTab.ToString() == "TabPage: {Feldkamp}") 
            //{

            //}
            //if (tabMetods.SelectedTab.ToString() == "TabPage: {MENT}") 
            //{
            //    for (int i = 0; i < Projection; i++) 
            //    {
            //        float[] an = new float[1];
            //        an[0] = angle[i];
            //        //Вызов алгоритма реконструкции
            //        //art.SARTFullScale(Projection, an, MassivPixels, ref recon, img_Size, gridBounds, gridSize, SizeX, SizeY, DistDetector);
            //    }
            //}
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            buttonRun.Enabled = false;
            panel1.Left = 0;
            panel1.Top = 0;
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            DeleteImageResourse();
        }

        private void DeleteImageResourse()
        {
            if (pic == null)
            {
                return;
            }

            for (int i = 0; i < projection; i++)
            {
                //Если ячейка массива не пуста, то удаляем изображение
                if (pic[i] != null)
                {
                    pic[i].Dispose();   //Освобождает все ресурсы занятыми pictureBox
                    File.Delete("pic" + i + ".bmp");//Удаление изображения из директории исполняемого файла
                }
            }
        }

        private void pictureBox1_MouseClick(object sender, MouseEventArgs e)
        {
            var imageForm = new ImageForm();
            //Передача изображения и активация формы ImageForm
            imageForm.img = pictureBox1.Image;
            imageForm.Show();
        }
    }
}
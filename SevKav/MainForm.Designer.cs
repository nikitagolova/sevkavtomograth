﻿namespace SevKav
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.buttonOpenRAWFile = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.progressBarImage = new System.Windows.Forms.ProgressBar();
            this.buttonRun = new System.Windows.Forms.Button();
            this.label23 = new System.Windows.Forms.Label();
            this.numericUpDownLayer = new System.Windows.Forms.NumericUpDown();
            this.label22 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.textBoxSizeX = new System.Windows.Forms.TextBox();
            this.textBoxSizeY = new System.Windows.Forms.TextBox();
            this.textBoxVoxelX = new System.Windows.Forms.TextBox();
            this.textBoxSizeZ = new System.Windows.Forms.TextBox();
            this.textBoxVoxelY = new System.Windows.Forms.TextBox();
            this.textBoxVoxelZ = new System.Windows.Forms.TextBox();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.textBoxMinAngle = new System.Windows.Forms.TextBox();
            this.tabMetods = new System.Windows.Forms.TabControl();
            this.tabART = new System.Windows.Forms.TabPage();
            this.tabFieldkamp = new System.Windows.Forms.TabPage();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.tabMENT = new System.Windows.Forms.TabPage();
            this.textBoxCountAngle = new System.Windows.Forms.TextBox();
            this.textBoxMaxAngle = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxWidth = new System.Windows.Forms.TextBox();
            this.textBoxDistObject = new System.Windows.Forms.TextBox();
            this.textBoxDistDetector = new System.Windows.Forms.TextBox();
            this.textBoxSizeWidth = new System.Windows.Forms.TextBox();
            this.textBoxHeight = new System.Windows.Forms.TextBox();
            this.textBoxSizeHeight = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.buttonOpenDCMFile = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.numericUpDownProjection = new System.Windows.Forms.NumericUpDown();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.flowLayoutPanelSliderImage = new System.Windows.Forms.FlowLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLayer)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.tabMetods.SuspendLayout();
            this.tabFieldkamp.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownProjection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(296, 0);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(325, 323);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseClick);
            // 
            // buttonOpenRAWFile
            // 
            this.buttonOpenRAWFile.Location = new System.Drawing.Point(75, 14);
            this.buttonOpenRAWFile.Margin = new System.Windows.Forms.Padding(2);
            this.buttonOpenRAWFile.Name = "buttonOpenRAWFile";
            this.buttonOpenRAWFile.Size = new System.Drawing.Size(98, 23);
            this.buttonOpenRAWFile.TabIndex = 1;
            this.buttonOpenRAWFile.Text = "Открыть файл";
            this.buttonOpenRAWFile.UseVisualStyleBackColor = true;
            this.buttonOpenRAWFile.Click += new System.EventHandler(this.btOpenFile_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.groupBox3);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(292, 724);
            this.panel1.TabIndex = 5;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.progressBarImage);
            this.groupBox3.Controls.Add(this.buttonRun);
            this.groupBox3.Controls.Add(this.label23);
            this.groupBox3.Controls.Add(this.numericUpDownLayer);
            this.groupBox3.Controls.Add(this.label22);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.label21);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.textBoxSizeX);
            this.groupBox3.Controls.Add(this.textBoxSizeY);
            this.groupBox3.Controls.Add(this.textBoxVoxelX);
            this.groupBox3.Controls.Add(this.textBoxSizeZ);
            this.groupBox3.Controls.Add(this.textBoxVoxelY);
            this.groupBox3.Controls.Add(this.textBoxVoxelZ);
            this.groupBox3.Controls.Add(this.listBox1);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.label20);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.label19);
            this.groupBox3.Location = new System.Drawing.Point(0, 402);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox3.Size = new System.Drawing.Size(290, 318);
            this.groupBox3.TabIndex = 16;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Reconstructor";
            // 
            // progressBarImage
            // 
            this.progressBarImage.Location = new System.Drawing.Point(0, 288);
            this.progressBarImage.Name = "progressBarImage";
            this.progressBarImage.Size = new System.Drawing.Size(290, 23);
            this.progressBarImage.TabIndex = 13;
            // 
            // buttonRun
            // 
            this.buttonRun.Location = new System.Drawing.Point(163, 63);
            this.buttonRun.Margin = new System.Windows.Forms.Padding(2);
            this.buttonRun.Name = "buttonRun";
            this.buttonRun.Size = new System.Drawing.Size(98, 21);
            this.buttonRun.TabIndex = 12;
            this.buttonRun.Text = "Run";
            this.buttonRun.UseVisualStyleBackColor = true;
            this.buttonRun.Click += new System.EventHandler(this.btRun_Click);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label23.Location = new System.Drawing.Point(8, 67);
            this.label23.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(48, 17);
            this.label23.TabIndex = 11;
            this.label23.Text = "Layer:";
            // 
            // numericUpDownLayer
            // 
            this.numericUpDownLayer.Location = new System.Drawing.Point(107, 65);
            this.numericUpDownLayer.Margin = new System.Windows.Forms.Padding(2);
            this.numericUpDownLayer.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.numericUpDownLayer.Name = "numericUpDownLayer";
            this.numericUpDownLayer.Size = new System.Drawing.Size(40, 20);
            this.numericUpDownLayer.TabIndex = 10;
            this.numericUpDownLayer.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numericUpDownLayer.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label22.Location = new System.Drawing.Point(265, 41);
            this.label22.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(20, 15);
            this.label22.TabIndex = 9;
            this.label22.Text = "px";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label18.Location = new System.Drawing.Point(265, 19);
            this.label18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(20, 15);
            this.label18.TabIndex = 9;
            this.label18.Text = "px";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label21.Location = new System.Drawing.Point(8, 41);
            this.label21.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(39, 17);
            this.label21.TabIndex = 8;
            this.label21.Text = "Size:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label17.Location = new System.Drawing.Point(9, 17);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(53, 17);
            this.label17.TabIndex = 8;
            this.label17.Text = "Voxels:";
            // 
            // textBoxSizeX
            // 
            this.textBoxSizeX.Location = new System.Drawing.Point(107, 40);
            this.textBoxSizeX.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxSizeX.Name = "textBoxSizeX";
            this.textBoxSizeX.Size = new System.Drawing.Size(42, 20);
            this.textBoxSizeX.TabIndex = 1;
            this.textBoxSizeX.Text = "400";
            this.textBoxSizeX.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxSizeY
            // 
            this.textBoxSizeY.Location = new System.Drawing.Point(164, 40);
            this.textBoxSizeY.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxSizeY.Name = "textBoxSizeY";
            this.textBoxSizeY.Size = new System.Drawing.Size(42, 20);
            this.textBoxSizeY.TabIndex = 1;
            this.textBoxSizeY.Text = "400";
            this.textBoxSizeY.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxVoxelX
            // 
            this.textBoxVoxelX.Location = new System.Drawing.Point(107, 17);
            this.textBoxVoxelX.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxVoxelX.Name = "textBoxVoxelX";
            this.textBoxVoxelX.Size = new System.Drawing.Size(42, 20);
            this.textBoxVoxelX.TabIndex = 1;
            this.textBoxVoxelX.Text = "256";
            this.textBoxVoxelX.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxSizeZ
            // 
            this.textBoxSizeZ.Location = new System.Drawing.Point(220, 40);
            this.textBoxSizeZ.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxSizeZ.Name = "textBoxSizeZ";
            this.textBoxSizeZ.Size = new System.Drawing.Size(42, 20);
            this.textBoxSizeZ.TabIndex = 1;
            this.textBoxSizeZ.Text = "400";
            this.textBoxSizeZ.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxVoxelY
            // 
            this.textBoxVoxelY.Location = new System.Drawing.Point(164, 17);
            this.textBoxVoxelY.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxVoxelY.Name = "textBoxVoxelY";
            this.textBoxVoxelY.Size = new System.Drawing.Size(42, 20);
            this.textBoxVoxelY.TabIndex = 1;
            this.textBoxVoxelY.Text = "256";
            this.textBoxVoxelY.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxVoxelZ
            // 
            this.textBoxVoxelZ.Location = new System.Drawing.Point(220, 17);
            this.textBoxVoxelZ.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxVoxelZ.Name = "textBoxVoxelZ";
            this.textBoxVoxelZ.Size = new System.Drawing.Size(42, 20);
            this.textBoxVoxelZ.TabIndex = 1;
            this.textBoxVoxelZ.Text = "256";
            this.textBoxVoxelZ.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(2, 89);
            this.listBox1.Margin = new System.Windows.Forms.Padding(2);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(291, 186);
            this.listBox1.TabIndex = 0;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label16.Location = new System.Drawing.Point(206, 19);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(15, 15);
            this.label16.TabIndex = 7;
            this.label16.Text = "X";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label20.Location = new System.Drawing.Point(206, 41);
            this.label20.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(15, 15);
            this.label20.TabIndex = 7;
            this.label20.Text = "X";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(149, 19);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(15, 15);
            this.label4.TabIndex = 6;
            this.label4.Text = "X";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label19.Location = new System.Drawing.Point(149, 41);
            this.label19.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(15, 15);
            this.label19.TabIndex = 6;
            this.label19.Text = "X";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.textBoxMinAngle);
            this.groupBox2.Controls.Add(this.tabMetods);
            this.groupBox2.Controls.Add(this.textBoxCountAngle);
            this.groupBox2.Controls.Add(this.textBoxMaxAngle);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.textBoxWidth);
            this.groupBox2.Controls.Add(this.textBoxDistObject);
            this.groupBox2.Controls.Add(this.textBoxDistDetector);
            this.groupBox2.Controls.Add(this.textBoxSizeWidth);
            this.groupBox2.Controls.Add(this.textBoxHeight);
            this.groupBox2.Controls.Add(this.textBoxSizeHeight);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(2, 93);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox2.Size = new System.Drawing.Size(288, 304);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Detector";
            // 
            // textBoxMinAngle
            // 
            this.textBoxMinAngle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxMinAngle.Location = new System.Drawing.Point(136, 115);
            this.textBoxMinAngle.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxMinAngle.Name = "textBoxMinAngle";
            this.textBoxMinAngle.Size = new System.Drawing.Size(38, 21);
            this.textBoxMinAngle.TabIndex = 15;
            this.textBoxMinAngle.Text = "-30";
            this.textBoxMinAngle.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tabMetods
            // 
            this.tabMetods.Controls.Add(this.tabART);
            this.tabMetods.Controls.Add(this.tabFieldkamp);
            this.tabMetods.Controls.Add(this.tabMENT);
            this.tabMetods.Location = new System.Drawing.Point(0, 136);
            this.tabMetods.Margin = new System.Windows.Forms.Padding(2);
            this.tabMetods.Name = "tabMetods";
            this.tabMetods.SelectedIndex = 0;
            this.tabMetods.Size = new System.Drawing.Size(288, 167);
            this.tabMetods.TabIndex = 15;
            // 
            // tabART
            // 
            this.tabART.Location = new System.Drawing.Point(4, 22);
            this.tabART.Margin = new System.Windows.Forms.Padding(2);
            this.tabART.Name = "tabART";
            this.tabART.Padding = new System.Windows.Forms.Padding(2);
            this.tabART.Size = new System.Drawing.Size(280, 141);
            this.tabART.TabIndex = 0;
            this.tabART.Text = "ART";
            this.tabART.UseVisualStyleBackColor = true;
            // 
            // tabFieldkamp
            // 
            this.tabFieldkamp.Controls.Add(this.label25);
            this.tabFieldkamp.Controls.Add(this.label24);
            this.tabFieldkamp.Controls.Add(this.comboBox3);
            this.tabFieldkamp.Controls.Add(this.comboBox2);
            this.tabFieldkamp.Location = new System.Drawing.Point(4, 22);
            this.tabFieldkamp.Margin = new System.Windows.Forms.Padding(2);
            this.tabFieldkamp.Name = "tabFieldkamp";
            this.tabFieldkamp.Padding = new System.Windows.Forms.Padding(2);
            this.tabFieldkamp.Size = new System.Drawing.Size(280, 141);
            this.tabFieldkamp.TabIndex = 1;
            this.tabFieldkamp.Text = "Feldkamp";
            this.tabFieldkamp.UseVisualStyleBackColor = true;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label25.Location = new System.Drawing.Point(40, 53);
            this.label25.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(75, 17);
            this.label25.TabIndex = 3;
            this.label25.Text = "Interptype:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label24.Location = new System.Drawing.Point(40, 22);
            this.label24.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(43, 17);
            this.label24.TabIndex = 2;
            this.label24.Text = "Filter:";
            // 
            // comboBox3
            // 
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Items.AddRange(new object[] {
            "linear",
            "nearest",
            "cubic",
            "spline"});
            this.comboBox3.Location = new System.Drawing.Point(133, 50);
            this.comboBox3.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(123, 21);
            this.comboBox3.TabIndex = 1;
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "ram-lak",
            "shepp-logan",
            "cosine",
            "hamming",
            "hann"});
            this.comboBox2.Location = new System.Drawing.Point(133, 19);
            this.comboBox2.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(123, 21);
            this.comboBox2.TabIndex = 0;
            // 
            // tabMENT
            // 
            this.tabMENT.Location = new System.Drawing.Point(4, 22);
            this.tabMENT.Margin = new System.Windows.Forms.Padding(2);
            this.tabMENT.Name = "tabMENT";
            this.tabMENT.Size = new System.Drawing.Size(280, 141);
            this.tabMENT.TabIndex = 2;
            this.tabMENT.Text = "MENT";
            this.tabMENT.UseVisualStyleBackColor = true;
            // 
            // textBoxCountAngle
            // 
            this.textBoxCountAngle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxCountAngle.Location = new System.Drawing.Point(182, 115);
            this.textBoxCountAngle.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxCountAngle.Name = "textBoxCountAngle";
            this.textBoxCountAngle.Size = new System.Drawing.Size(34, 21);
            this.textBoxCountAngle.TabIndex = 15;
            this.textBoxCountAngle.Text = "1";
            this.textBoxCountAngle.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxMaxAngle
            // 
            this.textBoxMaxAngle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxMaxAngle.Location = new System.Drawing.Point(225, 115);
            this.textBoxMaxAngle.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxMaxAngle.Name = "textBoxMaxAngle";
            this.textBoxMaxAngle.Size = new System.Drawing.Size(34, 21);
            this.textBoxMaxAngle.TabIndex = 15;
            this.textBoxMaxAngle.Text = "30";
            this.textBoxMaxAngle.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label15.Location = new System.Drawing.Point(259, 115);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(32, 15);
            this.label15.TabIndex = 14;
            this.label15.Text = "grad";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label14.Location = new System.Drawing.Point(260, 67);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(29, 15);
            this.label14.TabIndex = 13;
            this.label14.Text = "mm";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label13.Location = new System.Drawing.Point(259, 42);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(29, 15);
            this.label13.TabIndex = 12;
            this.label13.Text = "mm";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label12.Location = new System.Drawing.Point(259, 19);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(20, 15);
            this.label12.TabIndex = 11;
            this.label12.Text = "px";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(174, 115);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(10, 15);
            this.label2.TabIndex = 6;
            this.label2.Text = ":";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(190, 43);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(15, 15);
            this.label7.TabIndex = 6;
            this.label7.Text = "X";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(190, 20);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(15, 15);
            this.label6.TabIndex = 5;
            this.label6.Text = "X";
            // 
            // textBoxWidth
            // 
            this.textBoxWidth.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxWidth.Location = new System.Drawing.Point(208, 19);
            this.textBoxWidth.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxWidth.Name = "textBoxWidth";
            this.textBoxWidth.Size = new System.Drawing.Size(50, 21);
            this.textBoxWidth.TabIndex = 1;
            this.textBoxWidth.Text = "304";
            this.textBoxWidth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxDistObject
            // 
            this.textBoxDistObject.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxDistObject.Location = new System.Drawing.Point(192, 89);
            this.textBoxDistObject.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxDistObject.Name = "textBoxDistObject";
            this.textBoxDistObject.Size = new System.Drawing.Size(67, 21);
            this.textBoxDistObject.TabIndex = 1;
            this.textBoxDistObject.Text = "500";
            this.textBoxDistObject.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxDistDetector
            // 
            this.textBoxDistDetector.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxDistDetector.Location = new System.Drawing.Point(192, 66);
            this.textBoxDistDetector.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxDistDetector.Name = "textBoxDistDetector";
            this.textBoxDistDetector.Size = new System.Drawing.Size(67, 21);
            this.textBoxDistDetector.TabIndex = 1;
            this.textBoxDistDetector.Text = "1100";
            this.textBoxDistDetector.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxSizeWidth
            // 
            this.textBoxSizeWidth.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxSizeWidth.Location = new System.Drawing.Point(208, 42);
            this.textBoxSizeWidth.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxSizeWidth.Name = "textBoxSizeWidth";
            this.textBoxSizeWidth.Size = new System.Drawing.Size(50, 21);
            this.textBoxSizeWidth.TabIndex = 1;
            this.textBoxSizeWidth.Text = "459.5";
            this.textBoxSizeWidth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxHeight
            // 
            this.textBoxHeight.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxHeight.Location = new System.Drawing.Point(136, 19);
            this.textBoxHeight.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxHeight.Name = "textBoxHeight";
            this.textBoxHeight.Size = new System.Drawing.Size(50, 21);
            this.textBoxHeight.TabIndex = 0;
            this.textBoxHeight.Text = "247";
            this.textBoxHeight.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxSizeHeight
            // 
            this.textBoxSizeHeight.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxSizeHeight.Location = new System.Drawing.Point(136, 42);
            this.textBoxSizeHeight.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxSizeHeight.Name = "textBoxSizeHeight";
            this.textBoxSizeHeight.Size = new System.Drawing.Size(50, 21);
            this.textBoxSizeHeight.TabIndex = 0;
            this.textBoxSizeHeight.Text = "383.5";
            this.textBoxSizeHeight.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(216, 115);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(10, 15);
            this.label3.TabIndex = 16;
            this.label3.Text = ":";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.Location = new System.Drawing.Point(7, 20);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(132, 17);
            this.label8.TabIndex = 7;
            this.label8.Text = "Detector resolution:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.Location = new System.Drawing.Point(7, 42);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(95, 17);
            this.label9.TabIndex = 8;
            this.label9.Text = "Detector size:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.Location = new System.Drawing.Point(7, 67);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(155, 17);
            this.label10.TabIndex = 9;
            this.label10.Text = "Dist source to detector:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label11.Location = new System.Drawing.Point(7, 90);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(141, 17);
            this.label11.TabIndex = 10;
            this.label11.Text = "Dist source to object:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(7, 115);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 17);
            this.label1.TabIndex = 10;
            this.label1.Text = "Angular range:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.buttonOpenDCMFile);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.numericUpDownProjection);
            this.groupBox1.Controls.Add(this.buttonOpenRAWFile);
            this.groupBox1.Location = new System.Drawing.Point(2, 9);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(288, 80);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Projection";
            // 
            // buttonOpenDCMFile
            // 
            this.buttonOpenDCMFile.Location = new System.Drawing.Point(177, 14);
            this.buttonOpenDCMFile.Margin = new System.Windows.Forms.Padding(2);
            this.buttonOpenDCMFile.Name = "buttonOpenDCMFile";
            this.buttonOpenDCMFile.Size = new System.Drawing.Size(98, 23);
            this.buttonOpenDCMFile.TabIndex = 3;
            this.buttonOpenDCMFile.Text = "Выбрать папку";
            this.buttonOpenDCMFile.UseVisualStyleBackColor = true;
            this.buttonOpenDCMFile.Click += new System.EventHandler(this.buttonDCMOpenFile_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(7, 46);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(75, 17);
            this.label5.TabIndex = 2;
            this.label5.Text = "Projection:";
            // 
            // numericUpDownProjection
            // 
            this.numericUpDownProjection.Location = new System.Drawing.Point(214, 44);
            this.numericUpDownProjection.Margin = new System.Windows.Forms.Padding(2);
            this.numericUpDownProjection.Maximum = new decimal(new int[] {
            61,
            0,
            0,
            0});
            this.numericUpDownProjection.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownProjection.Name = "numericUpDownProjection";
            this.numericUpDownProjection.Size = new System.Drawing.Size(45, 20);
            this.numericUpDownProjection.TabIndex = 0;
            this.numericUpDownProjection.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numericUpDownProjection.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new System.Drawing.Point(634, 0);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(326, 323);
            this.pictureBox2.TabIndex = 7;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Location = new System.Drawing.Point(634, 328);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(326, 319);
            this.pictureBox3.TabIndex = 7;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Location = new System.Drawing.Point(296, 328);
            this.pictureBox4.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(325, 319);
            this.pictureBox4.TabIndex = 7;
            this.pictureBox4.TabStop = false;
            // 
            // flowLayoutPanelSliderImage
            // 
            this.flowLayoutPanelSliderImage.AutoScroll = true;
            this.flowLayoutPanelSliderImage.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanelSliderImage.Location = new System.Drawing.Point(296, 649);
            this.flowLayoutPanelSliderImage.Margin = new System.Windows.Forms.Padding(2);
            this.flowLayoutPanelSliderImage.Name = "flowLayoutPanelSliderImage";
            this.flowLayoutPanelSliderImage.Size = new System.Drawing.Size(664, 75);
            this.flowLayoutPanelSliderImage.TabIndex = 8;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(964, 725);
            this.Controls.Add(this.flowLayoutPanelSliderImage);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox4);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TomoSynthesis";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLayer)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabMetods.ResumeLayout(false);
            this.tabFieldkamp.ResumeLayout(false);
            this.tabFieldkamp.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownProjection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button buttonOpenRAWFile;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown numericUpDownProjection;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TabControl tabMetods;
        private System.Windows.Forms.TabPage tabART;
        private System.Windows.Forms.TabPage tabFieldkamp;
        private System.Windows.Forms.TabPage tabMENT;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxWidth;
        private System.Windows.Forms.TextBox textBoxDistObject;
        private System.Windows.Forms.TextBox textBoxDistDetector;
        private System.Windows.Forms.TextBox textBoxSizeWidth;
        private System.Windows.Forms.TextBox textBoxHeight;
        private System.Windows.Forms.TextBox textBoxSizeHeight;
        private System.Windows.Forms.TextBox textBoxMinAngle;
        private System.Windows.Forms.TextBox textBoxCountAngle;
        private System.Windows.Forms.TextBox textBoxMaxAngle;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.NumericUpDown numericUpDownLayer;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxSizeX;
        private System.Windows.Forms.TextBox textBoxSizeY;
        private System.Windows.Forms.TextBox textBoxVoxelX;
        private System.Windows.Forms.TextBox textBoxSizeZ;
        private System.Windows.Forms.TextBox textBoxVoxelY;
        private System.Windows.Forms.TextBox textBoxVoxelZ;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button buttonRun;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.Button buttonOpenDCMFile;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelSliderImage;
        private System.Windows.Forms.ProgressBar progressBarImage;
    }
}


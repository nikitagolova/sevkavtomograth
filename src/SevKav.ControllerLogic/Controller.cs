﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SevKav;
using SevKav.ReadFileEntity;

namespace SevKav.ControllerLogic
{
    public class Controller
    {
        MainForm mainForm;
        IReadEntity readEntity;

        public Controller()
        {
            mainForm = new MainForm();
            readEntity = new ReadFile();

            mainForm.OpenFile += MainForm_OpenFile;
            mainForm.OpenSomeFile += MainForm_OpenSomeFile;
        }

        private void MainForm_OpenSomeFile(object sender, EventArgs e)
        {
            //readEntity.OpenFilesEntity();
        }

        private void MainForm_OpenFile(object sender, EventArgs e)
        {
            readEntity.OpenFileEntity();
            mainForm.bitmap = readEntity._bmpFile;
        }
    }
}

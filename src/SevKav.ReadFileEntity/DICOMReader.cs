﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SevKav.ReadFileEntity
{
    public interface IDicomFileReader
    {
        string _name { get; set; }
        int _projection { get; set; }
        int _rows { get; set; }
        int _collums { get; set; }
        int _samples_Per_Pixel { get; set; }
        int _bits_Allocated { get; set; }
        int _bits_Stored { get; set; }
        int _pixel_Representation { get; set; }

        Bitmap CreateBitmap(string path);
        ushort[] Buffer(string[] paths);
    }

    class DICOMReader : IDicomFileReader
    {
        public string _name { get; set; }
        public int _rows { get; set; }
        public int _collums { get; set; }
        public int _samples_Per_Pixel { get; set; }
        public int _bits_Allocated { get; set; }
        public int _bits_Stored { get; set; }
        public int _pixel_Representation { get; set; }
        public int _projection { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public DICOMReader()
        {
            readFileEntity = new ReadFile();
        }

        public Bitmap CreateBitmap(string path)
        {
            byte[] AllBits = ReadDicomFile(path);
            ushort[] mas1 = new ushort[_pixel_Data.Length / 2];
            int k = 0;
            int winMin = 65536;
            int winMax = 0;

            _projection = 1;

            for (int i = 0; i < AllBits.Length - 1; i += 2)
            {
                mas1[k] = (ushort)(Convert.ToUInt16((AllBits[i + 1] << 8) + AllBits[i]));
                if (mas1[k] > winMax) winMax = mas1[k];
                if (mas1[k] < winMin) winMin = mas1[k];
                k++;
            }

            readFileEntity._massivFileData = mas1;

            int range = winMax - winMin;
            if (range < 1) range = 1;
            double factor = 255.0 / range;

            Bitmap bmp = new Bitmap(_collums, _rows, PixelFormat.Format24bppRgb);
            BitmapData bmd = bmp.LockBits(new Rectangle(0, 0, _collums, _rows), ImageLockMode.ReadOnly, bmp.PixelFormat);


            IntPtr xx;
            int bytes = mas1.Length * 3;
            byte[] rgbValues = new byte[bytes];
            int jk = 0;

            for (int i = 0; i < bmd.Height; i++)
            {
                xx = bmd.Scan0 + (i * bmd.Stride);

                for (int j = 0; j < bmd.Width; j++)
                {
                    rgbValues[jk + 0] = (byte)((mas1[i * bmd.Width + j] - winMin) * factor);
                    rgbValues[jk + 1] = (byte)((mas1[i * bmd.Width + j] - winMin) * factor);
                    rgbValues[jk + 2] = (byte)((mas1[i * bmd.Width + j] - winMin) * factor);
                    jk += 3;
                }

                System.Runtime.InteropServices.Marshal.Copy(rgbValues, i * bmd.Width * 3, xx, bmd.Width * 3);
            }

            bmp.UnlockBits(bmd);
            return bmp;
        }

        public ushort[] Buffer(string[] paths)
        {
            var collums = CheckCollums(paths);
            var rows = CheckRows(paths);
            byte[] oneTime;
            var outPut = new byte[2 * rows * collums * paths.Length];
            int k = 0;

            _projection = paths.Length;

            foreach (string path in paths)
            {
                oneTime = TreatmentDCM(path);
                for (int i = 0; i < oneTime.Length - 1; i++)
                {
                    outPut[k] = oneTime[i];
                    k++;
                }

            }

            var outputShort = new ushort[outPut.Length];

            for (int i=0; i<outPut.Length; i+=2)
            {
                outputShort[i / 2] = (ushort)(outPut[i] * 256 + outPut[i + 1]);
            }

            return outputShort;
        }

        private IReadEntity readFileEntity;
        private byte[] _pixel_Data;

        private byte[] ReadDicomFile(string Path)
        {
            EvilDicom.Components.DICOMFile dcmf = new EvilDicom.Components.DICOMFile(Path);
            EvilDicom.Components.DICOMElement dICOMElement = new EvilDicom.Components.DICOMElement();

            var tName = dcmf.Find(EvilDicom.Helper.TagHelper.PATIENT_NAME) as EvilDicom.VR.PersonsName;
            _name = tName.Data;

            var tCollums = dcmf.Find(EvilDicom.Helper.TagHelper.COLUMNS) as EvilDicom.VR.UnsignedShort;
            _collums = tCollums.Data;
            var trows = dcmf.Find(EvilDicom.Helper.TagHelper.ROWS) as EvilDicom.VR.UnsignedShort;
            _rows = trows.Data;

            var tspp = dcmf.Find(EvilDicom.Helper.TagHelper.SAMPLES_PER_PIXEL) as EvilDicom.VR.UnsignedShort;
            _samples_Per_Pixel = tspp.Data;

            var tba = dcmf.Find(EvilDicom.Helper.TagHelper.BITS_ALLOCATED) as EvilDicom.VR.UnsignedShort;
            _bits_Allocated = tba.Data;
            var tbs = dcmf.Find(EvilDicom.Helper.TagHelper.BITS_STORED) as EvilDicom.VR.UnsignedShort;
            _bits_Stored = tbs.Data;
            var tpr = dcmf.Find(EvilDicom.Helper.TagHelper.PIXEL_REPRESENTATION) as EvilDicom.VR.UnsignedShort;
            _pixel_Representation = tpr.Data;

            var tpd = dcmf.Find(EvilDicom.Helper.TagHelper.PIXEL_DATA) as EvilDicom.VR.PixelData;
            _pixel_Data = tpd.ByteData;
            var tmin = dcmf.Find(EvilDicom.Helper.TagHelper.CHANNEL_MAXIMUM_VALUE) as EvilDicom.VR.IntegerString;

            return _pixel_Data;
        }

        private int CheckCollums(string[] Paths)
        {
            int ChCollums = 0;
            bool flag = true;

            foreach (string Path in Paths)
            {
                EvilDicom.Components.DICOMFile dcmf = new EvilDicom.Components.DICOMFile(Path);
                EvilDicom.Components.DICOMElement dICOMElement = new EvilDicom.Components.DICOMElement();

                var tCollums = dcmf.Find(EvilDicom.Helper.TagHelper.COLUMNS) as EvilDicom.VR.UnsignedShort;
                var Collums = tCollums.Data;

                if (flag)
                {
                    ChCollums = (int)Collums;
                    flag = false;
                }

                if (ChCollums != (int)Collums) ;
                //Здесь выдавать ошибку
                //MessageBox.Show("Не совпадают разрешения выбранных Dicom файлов");
            }
            return ChCollums;
        }

        private int CheckRows(string[] Paths)
        {
            int ChRows = 0;
            bool flag = true;

            foreach (string Path in Paths)
            {
                EvilDicom.Components.DICOMFile dcmf = new EvilDicom.Components.DICOMFile(Path);
                EvilDicom.Components.DICOMElement dICOMElement = new EvilDicom.Components.DICOMElement();

                var trows = dcmf.Find(EvilDicom.Helper.TagHelper.ROWS) as EvilDicom.VR.UnsignedShort;
                var Rows = trows.Data;

                if (flag)
                {
                    ChRows = (int)Rows;
                    flag = false;
                }

                if (ChRows != (int)Rows) ;
                //Здесь выдавать ошибку
                //MessageBox.Show("Не совпадают разрешения выбранных Dicom файлов");
            }
            return ChRows;
        }

        private byte[] TreatmentDCM(string Path)
        {
            EvilDicom.Components.DICOMFile dcmf = new EvilDicom.Components.DICOMFile(Path);
            EvilDicom.Components.DICOMElement dICOMElement = new EvilDicom.Components.DICOMElement();

            var tpd = dcmf.Find(EvilDicom.Helper.TagHelper.PIXEL_DATA) as EvilDicom.VR.PixelData;
            var Pixel_Data = tpd.ByteData;

            return Pixel_Data;
        }
    }
}

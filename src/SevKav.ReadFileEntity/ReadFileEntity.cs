﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;
using System.Drawing.Imaging;

namespace SevKav.ReadFileEntity
{
    public interface IReadEntity
    {
        Bitmap _bmpFile { get; set; }
        ushort[] _massivFileData { get; set; }
        IDicomFileReader dcmReader { get; set; }

        void OpenFileEntity();
        void OpenFilesEntity(int projection);
    }

    public class ReadFile: IReadEntity
    {
        public Bitmap _bmpFile { get; set; }
        public ushort[] _massivFileData { get; set; }
        public IDicomFileReader dcmReader { get; set; }

        public ReadFile()
        {
            dcmReader = new DICOMReader();
        }

        public void OpenFileEntity()
        {
            var ofd = new OpenFileDialog();

            //отображать предупреждение, если пользователь указывает несуществующий путь
            ofd.CheckPathExists = true;
            //отображается кнопка "Справка" в диалоговом окне
            ofd.ShowHelp = true;
            //Настраиваем фильтр на необходимые форматы файлов
            ofd.Filter = "RAW Files(*.RAW)|*.RAW|DICOM files (*.DCM)|*.DCM";

            if (ofd.ShowDialog() != DialogResult.OK)
            {
                return;
            }

            if (ofd.FilterIndex == 1)
            {
                _bmpFile = ReadRawFile(ofd);
            }
            else if (ofd.FilterIndex == 2)
            {
                _bmpFile = ReadDicomFile(ofd);
            }

        }

        public void OpenFilesEntity(int projection)
        {
            throw new NotImplementedException();
        }

        private Bitmap ReadRawFile(OpenFileDialog ofd)
        {
            var _massivRawByte = File.ReadAllBytes(ofd.FileName);

            int _widthRaw = 3040;
            int _heightRaw = 2466;

            var bmpRaw = new Bitmap(_widthRaw, _heightRaw, PixelFormat.Format48bppRgb);
            var rectangle = new Rectangle(0, 0, _widthRaw, _heightRaw);
            var bmpData = bmpRaw.LockBits(rectangle, ImageLockMode.WriteOnly, bmpRaw.PixelFormat);

            var _ptr = bmpData.Scan0;

            // объявление байтового массива размером с изображение
            var bytes = bmpData.Stride * bmpRaw.Height;
            var rgbValues = new byte[bytes];

            var _massivRawPixel = new ushort[bytes];

            int k = 0;

            for (int i = 0; i < rgbValues.Length; i += 6)
            {
                rgbValues[i + 5] = (byte)(_massivRawByte[k + 1]);
                rgbValues[i + 4] = (byte)(_massivRawByte[k] * 256);
                rgbValues[i + 3] = (byte)(_massivRawByte[k + 1]);
                rgbValues[i + 2] = (byte)(_massivRawByte[k] * 256);
                rgbValues[i + 1] = (byte)(_massivRawByte[k + 1]);
                rgbValues[i] = (byte)(_massivRawByte[k] * 256);

                _massivRawPixel[k / 2] = (ushort)(_massivRawByte[k] * 256 + _massivRawByte[k + 1]);

                k += 2;
            }

            _massivFileData = _massivRawPixel;

            //копирование матрицы обратнов изображение
            System.Runtime.InteropServices.Marshal.Copy(rgbValues, 0, _ptr, bytes);

            bmpRaw.UnlockBits(bmpData);
            bmpRaw.RotateFlip(RotateFlipType.Rotate270FlipXY);
            return bmpRaw;
        }

        private Bitmap ReadDicomFile(OpenFileDialog ofd)
        {
            var _bmpDicom = dcmReader.CreateBitmap(ofd.FileName);

            return _bmpDicom;
        }
    }
}

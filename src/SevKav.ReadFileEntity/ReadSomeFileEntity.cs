﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SevKav.ReadFileEntity
{
    class ReadSomeFileEntity:IReadEntity
    {

        public Bitmap _bmpFile { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public ushort[] _massivFileData { get; set; }
        public IDicomFileReader dcmReader { get; set; }

        public ReadSomeFileEntity()
        {
            dcmReader = new DICOMReader();
        }

        public void OpenFileEntity()
        {
            throw new NotImplementedException();
        }

        public void OpenFilesEntity(int projection)
        {
            var fbd = new FolderBrowserDialog();

            if (fbd.ShowDialog() != DialogResult.OK)
            {
                return;
            }

            string[] fullfilesPath = Directory.GetFiles(fbd.SelectedPath, "*.dcm"); //массив директорий DICOM файлов на диске
            
            if (fullfilesPath.Length == 0)
            {
                return;
            }

            if (projection > fullfilesPath.Length)
            {
                projection = fullfilesPath.Length;
            }

            _massivFileData = dcmReader.Buffer(fullfilesPath);
            dcmReader._projection = projection;
        }
    }
}
